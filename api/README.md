# ts-express-seed

This is the Express API template I use for my projects. CTO [Alex Permiakov](https://itnext.io/production-ready-node-js-rest-apis-setup-using-typescript-postgresql-and-redis-a9525871407).

This template can also be easily converted into a Serverless API with AWS Lambda using the Serverless Framework.

### Start Server Locally

`npm start`

### Unit test

Test is done using Jest. I test my code with integration and unit tests, but for demo purposes I only did integration tests :)


Run

`npm test`
