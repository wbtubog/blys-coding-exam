import express from 'express';
import middleware from './middlewares';
import errorHandlers from './middlewares/error-handlers.middleware';
import routes from './services';
import { applyMiddleware, applyRoutes } from './utils';


const server = express();
const PORT = process.env.PORT || 8000;
applyMiddleware(middleware, server);
applyRoutes(routes, server);
applyMiddleware(errorHandlers, server);


server.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`)
})