import { applyMiddleware, applyRoutes } from '../../../../utils';
import routes from '../../../../services/auth/routes';
import express from 'express';
import middleware from './../../../../middlewares';
import errorHandlers from './../../../../middlewares/error-handlers.middleware';
import { agent as request } from 'supertest';

// Sample routes

describe('Auth Routes', () => {
  let server;
  beforeAll(() => {
    server = express();

    applyMiddleware(middleware, server);
    applyRoutes(routes, server);
    applyMiddleware(errorHandlers, server);
  });

  describe('/auth/verify', () => {
    it('Should return HTTP 400 error when `code` is not present in request body', (done) => {
      request(server).post('/auth/verify').send({}).expect(400, done);
    });

    it('Should return HTTP 400 error when `code` is greater than or less than 6 digits', (done) => {
      request(server)
        .post('/auth/verify')
        // 7 digit code
        .send({ code: '1234567' })
        .expect(400, done);
    });

    it('Should return HTTP 400 error when `code` last digit is 7', (done) => {
      request(server)
        .post('/auth/verify')
        // 7 digit code
        .send({ code: '123457' })
        .expect(400, done);
    });

    it('Should return HTTP 200 when `code` is valid', (done) => {
      request(server)
        .post('/auth/verify')
        .send({ code: '091294' })
        .expect(200, done);
    });
  });
});
