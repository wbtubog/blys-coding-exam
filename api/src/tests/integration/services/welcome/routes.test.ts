import { applyMiddleware, applyRoutes } from '../../../../utils';
import routes from '../../../../services/welcome/routes';
import express from 'express';
import middleware from './../../../../middlewares';
import errorHandlers from './../../../../middlewares/error-handlers.middleware';
import { agent as request } from 'supertest';

// Sample routes

describe('Welcome Routes', () => {
  let server;
  beforeAll(() => {
    server = express();

    applyMiddleware(middleware, server);
    applyRoutes(routes, server);
    applyMiddleware(errorHandlers, server);
  });

  it('Should pass', (done) => {
    request(server).get('/welcome').expect(200, { message: 'Welcome!' }, done);
  });
});
