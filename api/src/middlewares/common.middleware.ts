import parser from 'body-parser';
import cors from 'cors';
import { Router } from 'express';
import helmet from 'helmet';
import nocache from 'nocache';

export const handleCors = (router: Router) => {
  router.use(cors({ origin: true }));
  router.use(helmet());
  router.use(nocache());
};

export const handleBodyRequestParsing = (router: Router) => {
  router.use(parser.urlencoded({ extended: true, limit: '10mb' }));
  router.use(parser.json({ limit: '10mb' }));
  // router.use(compression({ debug: true }));
};
