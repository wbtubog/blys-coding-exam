import { handleCors, handleBodyRequestParsing } from './common.middleware';

export default [handleCors, handleBodyRequestParsing];
