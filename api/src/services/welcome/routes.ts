import { Route } from '../../interfaces/request.interface';

export default [
  {
    path: '/welcome',
    method: 'get',
    handler: [
      (req, res, next) => {
        res.send({ message: 'Welcome!' });
      },
    ],
  },
] as Route[];
