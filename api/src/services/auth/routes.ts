import { Route } from '../../interfaces/request.interface';
import { HTTP400Error } from '../../utils/http-errors';

export default [
  {
    path: '/auth/verify',
    method: 'post',
    handler: [
      (req, res, next) => {
        try {
          const { code } = req.body;
          if (!code) {
            throw new HTTP400Error({
              message: 'code is not present in the request body',
            });
          }
          if (code.length != 6 || code[code.length - 1] === '7') {
            throw new HTTP400Error({
              message: 'invalid code',
            });
          }

          res.send({ code });
        } catch (e) {
          next(e);
        }
      },
    ],
  },
] as Route[];
