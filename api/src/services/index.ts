import welcomeRoutes from './welcome/routes';
import authRoutes from './auth/routes';

export default [
  ...welcomeRoutes,
  ...authRoutes
];
