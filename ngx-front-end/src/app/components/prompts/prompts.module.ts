import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AlertComponent } from './alert/alert.component';
import { LoaderComponent } from './loader/loader.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule } from '@angular/material/button';
@NgModule({
  declarations: [AlertComponent, LoaderComponent],
  imports: [CommonModule, MatProgressSpinnerModule, MatButtonModule],
  exports: [AlertComponent, LoaderComponent],
})
export class PromptsModule {}
