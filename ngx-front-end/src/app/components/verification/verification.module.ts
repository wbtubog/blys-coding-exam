import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VerificationComponent } from './verification.component';
import { CodeInputComponent } from './code-input/code-input.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [VerificationComponent, CodeInputComponent],
  imports: [CommonModule, FormsModule],
  exports: [VerificationComponent],
})
export class VerificationModule {}
