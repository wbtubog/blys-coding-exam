import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { CodeInputComponent } from './code-input/code-input.component';

import { VerificationComponent } from './verification.component';

describe('VerificationComponent', () => {
  let component: VerificationComponent;
  let fixture: ComponentFixture<VerificationComponent>;
  const codeInputs = [
    {
      setValue: (v) => {},
      getValue: () => '1',
      validate: () => true,
      focus: () => () => {},
      blur: () => () => {},
      value: '1',
    },
    {
      setValue: (v) => {},
      getValue: () => '2',
      validate: () => true,
      focus: () => () => {},
      blur: () => () => {},
      value: '2',
    },
  ];
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VerificationComponent, CodeInputComponent],
      imports: [],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.digits = 2;

    component.codeInputs = {
      // @ts-ignore
      toArray: () => {
        return codeInputs;
      },
    };
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#handleInput', () => {
    it('should focus on the next CodeInputComponent', () => {
      spyOn(codeInputs[0], 'blur');
      spyOn(codeInputs[1], 'focus');

      component.handleInput({
        idx: 0,
        isBackwards: false,
      });

      expect(codeInputs[0].blur).toHaveBeenCalled();
      expect(codeInputs[1].focus).toHaveBeenCalled();
    });

    it('should focus on the previous CodeInputComponent if backspace was pressed', () => {
      spyOn(codeInputs[1], 'blur');
      spyOn(codeInputs[0], 'focus');

      component.handleInput({
        idx: 1,
        isBackwards: true,
      });

      expect(codeInputs[0].focus).toHaveBeenCalled();
      expect(codeInputs[1].blur).toHaveBeenCalled();
    });

    it('should ignore focus and blur calls if the next CodeInputComponent index does not exist', () => {
      spyOn(codeInputs[1], 'blur');

      component.handleInput({
        idx: 1,
        isBackwards: false,
      });

      expect(codeInputs[1].blur).not.toHaveBeenCalled();
    });
  });

  describe('#handlePaste', () => {
    it('should set the value for each of the CodeInputComponents', fakeAsync(() => {
      spyOn(codeInputs[0], 'setValue');
      spyOn(codeInputs[1], 'setValue');
      component.handlePaste({
        value: '12',
      });
      tick(10);
      expect(codeInputs[0].setValue).toHaveBeenCalledWith('1');
      expect(codeInputs[1].setValue).toHaveBeenCalledWith('2');
    }));

    it('should ignore the excess character when pasting', fakeAsync(() => {
      spyOn(codeInputs[0], 'setValue');
      spyOn(codeInputs[1], 'setValue');
      component.handlePaste({
        // component has been configured to cater 2 digits only
        value: '123',
      });
      tick(10);
      expect(codeInputs[0].setValue).toHaveBeenCalledWith('1');
      expect(codeInputs[1].setValue).toHaveBeenCalledWith('2');
    }));
  });

  describe('#getValue', () => {
    it('should combine all the values of CodeInputComponents into 1 string', () => {
      expect(component.getValue()).toEqual('12');
    });

    it('should return null if one of the CodeInputComponents is invalid', () => {
      const _codeInputs = [...codeInputs];
      _codeInputs[1] = {
        ..._codeInputs[1],
        validate: () => false,
      };
      component.codeInputs = {
        // @ts-ignore
        toArray: () => {
          return [..._codeInputs];
        },
      };

      fixture.detectChanges();

      expect(component.getValue()).toBeNull();
    });
  });
});
