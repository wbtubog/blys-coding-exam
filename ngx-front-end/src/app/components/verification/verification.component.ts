import {
  Component,
  Input,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { CodeInputComponent } from './code-input/code-input.component';
import {
  CodeInputEvent,
  CodeInputPasteEvent,
} from './code-input/code-input.interface';

@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.scss'],
})
export class VerificationComponent implements OnInit {
  /**
   * How many digits should the verification component
   * allow?
   */
  @Input()
  digits: number;

  /**
   * Placeholder for ngFor loop
   */
  inputs: number[] = [];

  @ViewChildren(CodeInputComponent)
  codeInputs: QueryList<CodeInputComponent>;

  constructor() {}

  ngOnInit(): void {
    this.inputs = Array(this.digits)
      .fill(0)
      .map((x, i) => i);
  }

  handleInput(ev: CodeInputEvent) {
    const inputs = this.codeInputs.toArray();
    const newIdx = ev.isBackwards ? ev.idx - 1 : ev.idx + 1;
    if (newIdx in inputs) {
      inputs[ev.idx].blur();
      inputs[newIdx].focus();
    }
  }

  handlePaste(ev: CodeInputPasteEvent) {
    const inputs = this.codeInputs.toArray();
    const chars = ev.value
      .split('')
      .map((v) => v.trim())
      .filter((v) => v !== '');

    setTimeout(() => {
      chars.forEach((value, index) => {
        if (index in inputs) {
          inputs[index].setValue(value);
        }
      });

      // after paste event, focus on the last node
      inputs[inputs.length - 1].focus();
    }, 10);
  }

  getValue() {
    const inputs = this.codeInputs.toArray();

    const isOneFieldInvalid = inputs
      .map((component) => {
        return component.validate();
      })
      .some((status) => status == false);

    return !isOneFieldInvalid ? inputs.map((c) => c.value).join('') : null;
  }
}
