import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { CodeInputEvent, CodeInputPasteEvent } from './code-input.interface';
import * as keyboardKey from 'keyboard-key';
@Component({
  selector: 'app-code-input',
  templateUrl: './code-input.component.html',
  styleUrls: ['./code-input.component.scss'],
})
export class CodeInputComponent implements OnInit {
  @Input()
  idx: number;

  @Output()
  onInput: EventEmitter<CodeInputEvent> = new EventEmitter();

  @Output()
  onPaste: EventEmitter<CodeInputPasteEvent> = new EventEmitter();

  @ViewChild('inpt', { read: ElementRef })
  inputMain: ElementRef<HTMLInputElement>;

  /**
   * Internal state values
   */
  private currentValue = '';
  private prevValue = '';

  isValid = true;

  constructor() {}

  ngOnInit(): void {}

  handleKeyup(event): void {
    event.preventDefault();
    const key = keyboardKey.getCode(event);

    // Handle backspace
    if (key == keyboardKey.Backspace) {
      this.prevValue = this.currentValue;
      this.currentValue = event.target.value;

      if (this.prevValue !== '') {
        return;
      }
      this.inputMain.nativeElement.value = '';

      this.onInput.emit({
        idx: this.idx,
        isBackwards: true,
      });

      return;
    }

    // Accept numeric chars
    if (
      (event.key != 'Meta' &&
        ((key >= 48 && key <= 57) ||
          (key >= 65 && key <= 90) ||
          (key >= 97 && key <= 122))) ||
      (key >= 186 && key <= 222)
    ) {
      this.inputMain.nativeElement.value = event.key;
      this.currentValue = event.target.value;
      this.onInput.emit({
        idx: this.idx,
        isBackwards: false,
      });
    }
  }

  handlePasteEvent(event) {
    setTimeout(() => {
      this.onPaste.emit({
        value: this.inputMain.nativeElement.value,
      });
      this.inputMain.nativeElement.value = '';
    }, 1);
  }

  focus(): void {
    this.inputMain.nativeElement.focus();
  }

  blur(): void {
    this.inputMain.nativeElement.blur();
  }

  setValue(value) {
    this.currentValue = value;
    this.prevValue = '';
    this.inputMain.nativeElement.value = value;
  }

  validate() {
    this.isValid = true;
    if (
      this.inputMain.nativeElement.value === '' ||
      isNaN(+this.inputMain.nativeElement.value)
    ) {
      this.isValid = false;
    }
    return this.isValid;
  }

  get value() {
    return this.inputMain.nativeElement.value;
  }
}
