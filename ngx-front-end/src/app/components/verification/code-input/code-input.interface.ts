export interface CodeInputEvent {
  idx: number;
  isBackwards: boolean;
}

export interface CodeInputPasteEvent {
  value: string;
}
