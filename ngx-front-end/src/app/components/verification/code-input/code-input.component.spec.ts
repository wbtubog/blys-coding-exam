import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { CodeInputComponent } from './code-input.component';
import * as keyboardKey from 'keyboard-key';
import { P } from 'keyboard-key';

describe('CodeInputComponent', () => {
  let component: CodeInputComponent;
  let fixture: ComponentFixture<CodeInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CodeInputComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#handleKeyup', () => {
    describe('Backspace Handler', () => {
      it('Should emit onInput if the text field is empty when backspace is pressed', () => {
        spyOn(component.onInput, 'emit');
        component.idx = 1;
        fixture.detectChanges();

        component.handleKeyup({
          which: keyboardKey.Backspace,
          preventDefault: () => {},
          target: { value: '1' },
        });

        expect(component.onInput.emit).toHaveBeenCalledWith({
          idx: 1,
          isBackwards: true,
        });
      });

      it('Should not emit onInput if the user just removed the previous value', () => {
        spyOn(component.onInput, 'emit');
        component.idx = 1;

        fixture.detectChanges();

        component.setValue('1');

        component.handleKeyup({
          which: keyboardKey.Backspace,
          preventDefault: () => {},
          target: { value: '' },
        });

        fixture.detectChanges();

        expect(component.onInput.emit).not.toHaveBeenCalled();
      });
    });

    describe('Character input handler', () => {
      it('should emit onInput whenever a numeric character key was pressed', () => {
        spyOn(component.onInput, 'emit');
        component.idx = 1;
        fixture.detectChanges();
        component.handleKeyup({
          which: keyboardKey['1'],
          preventDefault: () => {},
          target: { value: '1' },
        });

        fixture.detectChanges();
        expect(component.onInput.emit).toHaveBeenCalledWith({
          idx: 1,
          isBackwards: false,
        });
      });

      it('should emit onInput whenever a character key from range 65-90 was pressed', () => {
        spyOn(component.onInput, 'emit');
        component.idx = 1;
        fixture.detectChanges();
        component.handleKeyup({
          which: 65,
          preventDefault: () => {},
          target: { value: '1' },
        });

        fixture.detectChanges();
        expect(component.onInput.emit).toHaveBeenCalledWith({
          idx: 1,
          isBackwards: false,
        });
      });

      it('should emit onInput whenever a character key from range 97-122 was pressed', () => {
        spyOn(component.onInput, 'emit');
        component.idx = 1;
        fixture.detectChanges();
        component.handleKeyup({
          which: 97,
          preventDefault: () => {},
          target: { value: '1' },
        });

        fixture.detectChanges();
        expect(component.onInput.emit).toHaveBeenCalledWith({
          idx: 1,
          isBackwards: false,
        });
      });

      it('should emit onInput whenever a character key from range 186-222 was pressed', () => {
        spyOn(component.onInput, 'emit');
        component.idx = 1;
        fixture.detectChanges();
        component.handleKeyup({
          which: 186,
          preventDefault: () => {},
          target: { value: '1' },
        });

        fixture.detectChanges();
        expect(component.onInput.emit).toHaveBeenCalledWith({
          idx: 1,
          isBackwards: false,
        });
      });

      it('Should not emit onInput if the key code is Meta', () => {
        spyOn(component.onInput, 'emit');
        component.idx = 1;
        fixture.detectChanges();

        component.handleKeyup({
          which: 'Meta',
          preventDefault: () => {},
          target: { value: 'Meta' },
        });

        expect(component.onInput.emit).not.toHaveBeenCalled();
      });
    });
  });

  describe('#handlePasteEvent', () => {
    it('should emit onPaste after 1ms', fakeAsync(() => {
      spyOn(component.onPaste, 'emit');
      component.setValue('13456');
      component.handlePasteEvent({});
      tick(1);
      expect(component.onPaste.emit).toHaveBeenCalledWith({
        value: '13456',
      });
    }));
  });

  describe('#focus', () => {
    it('should set the focus on the input element', () => {
      spyOn(component.inputMain.nativeElement, 'focus');
      component.focus();
      expect(component.inputMain.nativeElement.focus).toHaveBeenCalled();
    });
  });

  describe('#blur', () => {
    it('should remove focus on the input element', () => {
      spyOn(component.inputMain.nativeElement, 'blur');
      component.blur();
      expect(component.inputMain.nativeElement.blur).toHaveBeenCalled();
    });
  });

  describe('#validate', () => {
    it('should return true if input has a value', () => {
      component.setValue('1');
      fixture.detectChanges();

      expect(component.validate()).toBeTruthy();
    });

    it('should return true if input has no value', () => {
      component.setValue('');
      fixture.detectChanges();

      expect(component.validate()).toBeFalsy();
    });
  });

  describe('#getValue', () => {
    it('Should return whatever the value of the input', () => {
      component.setValue('1');
      fixture.detectChanges();

      expect(component.value).toEqual('1');
    });
  });
});
