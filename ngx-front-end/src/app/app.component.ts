import { Component, OnInit, ViewChild } from '@angular/core';
import { VerificationComponent } from './components/verification/verification.component';
import { AuthService } from './services/auth.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  @ViewChild(VerificationComponent)
  verificationComponent: VerificationComponent;

  title = 'ngx-front-end';

  constructor(private authService: AuthService) {}

  ngOnInit() {}

  submit() {
    const code = this.verificationComponent.getValue();
    if (code) {
      this.authService.verifyCode(code).subscribe();
    }
  }
}
