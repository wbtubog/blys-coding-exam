import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { AlertsService } from './alerts.service';
import { environment } from 'src/environments/environment';

describe('AuthService', () => {
  let service: AuthService;
  let httpMock: HttpTestingController;

  const alertsServiceSpy = jasmine.createSpyObj('AlertServiceSpy', [
    'showLoading',
    'showAlert',
  ]);
  const closeSpy = jasmine.createSpy();
  alertsServiceSpy.showLoading.and.callFake(() => ({ close: closeSpy }));

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: AlertsService,
          useValue: alertsServiceSpy,
        },
      ],
    });
    service = TestBed.inject(AuthService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  afterEach(() => {
    alertsServiceSpy.showLoading.calls.reset();
    alertsServiceSpy.showAlert.calls.reset();
  })

  describe('#verifyCode', () => {
    it('should successfully verify code', () => {
      const code = '091299';
      service.verifyCode(code).subscribe(() => {
        expect(alertsServiceSpy.showLoading).toHaveBeenCalled();
        expect(alertsServiceSpy.showAlert).toHaveBeenCalledWith('Success!');
      });

      const req = httpMock.expectOne(`${environment.api}/auth/verify`);
      expect(req.request.method).toEqual('POST');
      expect(req.request.body).toEqual({ code });

      req.flush({});
    });

    it('should handle HTTP 400 error', () => {
      const code = '091299';
      service.verifyCode(code).subscribe(
        () => {},
        () => {
          expect(alertsServiceSpy.showLoading).toHaveBeenCalled();
          expect(alertsServiceSpy.showAlert).toHaveBeenCalledWith(
            'Invalid code'
          );
        }
      );

      const req = httpMock.expectOne(`${environment.api}/auth/verify`);
      expect(req.request.method).toEqual('POST');
      expect(req.request.body).toEqual({ code });

      req.flush({}, { status: 400, statusText: 'Bad Request' });
    });

    it('should any errors aside from http 400', () => {
      const code = '091299';
      service.verifyCode(code).subscribe(
        () => {},
        () => {
          expect(alertsServiceSpy.showLoading).toHaveBeenCalled();
          expect(alertsServiceSpy.showAlert).toHaveBeenCalledWith(
            'Something went wrong, please try again.'
          );
        }
      );

      const req = httpMock.expectOne(`${environment.api}/auth/verify`);
      expect(req.request.method).toEqual('POST');
      expect(req.request.body).toEqual({ code });

      req.flush({}, { status: 500, statusText: 'Internal Server Error' });
    });
  });
});
