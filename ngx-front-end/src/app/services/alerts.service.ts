import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AlertComponent } from '../components/prompts/alert/alert.component';
import { LoaderComponent } from '../components/prompts/loader/loader.component';
@Injectable({
  providedIn: 'root',
})
export class AlertsService {
  constructor(private modal: MatDialog) {}

  showLoading() {
    return this.modal.open(LoaderComponent, {
      disableClose: true,
    });
  }

  showAlert(message: string) {
    return this.modal.open(AlertComponent, {
      data: {
        message,
      },
    });
  }
}
