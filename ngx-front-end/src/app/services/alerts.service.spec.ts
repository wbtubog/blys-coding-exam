import { TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { AlertComponent } from '../components/prompts/alert/alert.component';
import { LoaderComponent } from '../components/prompts/loader/loader.component';

import { AlertsService } from './alerts.service';

describe('AlertsService', () => {
  let service: AlertsService;

  const matDialogSpy = jasmine.createSpyObj('MatDialogSpy', ['open'])

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: MatDialog,
          useValue: matDialogSpy
        }
      ]
    });
    service = TestBed.inject(AlertsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#showAlert', () => {
    it('should open the modal and pass the necessary config', () => {
      service.showAlert('Hello');
      expect(matDialogSpy.open).toHaveBeenCalledWith(AlertComponent, {
        data: {
          message: 'Hello'
        }
      })
    });
  })

  describe('#showLoading', () => {
    it('should open the modal and pass the necessary config', () => {
      service.showLoading();
      expect(matDialogSpy.open).toHaveBeenCalledWith(LoaderComponent, {
        disableClose: true
      })
    });
  })
});
