import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { AlertsService } from './alerts.service';
import { catchError, tap } from 'rxjs/operators';
import { of, throwError } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient, private alertsService: AlertsService) {}

  verifyCode(code: string) {
    const loader = this.alertsService.showLoading();
    return this.http.post(`${environment.api}/auth/verify`, { code }).pipe(
      tap(() => {
        loader.close();
        this.alertsService.showAlert('Success!');
      }),
      catchError((e) => {
        loader.close();
        if (e.status === 400) {
          this.alertsService.showAlert('Invalid code');
        } else {
          this.alertsService.showAlert(
            'Something went wrong, please try again.'
          );
        }
        return throwError(e);
      })
    );
  }
}
