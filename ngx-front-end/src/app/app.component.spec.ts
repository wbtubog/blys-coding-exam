import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { AuthService } from './services/auth.service';
import { VerificationModule } from './components/verification/verification.module';
import { of } from 'rxjs';
describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  const authServiceSpy = jasmine.createSpyObj('AuthService', ['verifyCode']);
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, VerificationModule],
      declarations: [AppComponent],
      providers: [
        {
          provide: AuthService,
          useValue: authServiceSpy,
        },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  afterEach(() => {
    authServiceSpy.verifyCode.calls.reset();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;

    expect(app).toBeTruthy();
  });

  describe('#submit', () => {
    it('should submit the verification code', () => {
      //@ts-ignore
      // mock
      component.verificationComponent = {
        getValue: () => '12',
      };

      authServiceSpy.verifyCode.and.callFake(() => {
        return of({});
      });
      component.submit();
      expect(authServiceSpy.verifyCode).toHaveBeenCalledWith('12');
    });

    it('should not submit if verification code is invalid', () => {
      //@ts-ignore
      // this will return null if some of the inputs are invalid
      component.verificationComponent = {
        getValue: () => null,
      };

      authServiceSpy.verifyCode.and.callFake(() => {
        return of({});
      });
      component.submit();
      expect(authServiceSpy.verifyCode).not.toHaveBeenCalled();
    });
  });
});
