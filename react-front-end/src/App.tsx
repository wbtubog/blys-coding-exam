import React from 'react';
import './App.css';
import Verification from './components/verification/Verification';

import { Button, Dialog } from '@material-ui/core';
import axios from 'axios';

export class App extends React.Component<
  {},
  { modalMessage: string; showModal: boolean }
> {
  private verificationComponentRef = React.createRef<Verification>();

  constructor(props) {
    super(props);

    this.state = {
      modalMessage: '',
      showModal: false,
    };
  }

  render() {
    return (
      <div className="App">
        <div className="verification">
          <h1>Verify Code</h1>
          <Verification ref={this.verificationComponentRef} digits={6} />
          <Button
            onClick={this.submit.bind(this)}
            className="submit-btn"
            variant="contained"
            color="primary"
          >
            SUBMIT
          </Button>
        </div>

        <Dialog
          open={this.state.showModal}
          onClose={() => {
            this.setState({ showModal: false });
          }}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
        >
          <div className="modal-hldr">{this.state.modalMessage}</div>
        </Dialog>
      </div>
    );
  }

  async submit() {
    const code = this.verificationComponentRef.current?.getValue();
    if (code) {
      try {
        await axios.post(`${process.env.REACT_APP_API}/auth/verify`, { code });
        this.setState({
          showModal: true,
          modalMessage: 'Success!',
        });
      } catch (e) {
        if ('response' in e) {
          if (e.response.status == 400) {
            this.setState({
              showModal: true,
              modalMessage: 'Invalid code!',
            });
          }
        } else {
          this.setState({
            showModal: true,
            modalMessage: 'Something went wrong, please try again',
          });
        }
      }
    }
  }
}
