import React from 'react';
import './CodeInput.css';
import { CodeInputProps, CodeInputState } from './CodeInput.interface';
import * as keyboardKey from 'keyboard-key';
import { eventNames } from 'cluster';

export default class CodeInput extends React.Component<
  CodeInputProps,
  CodeInputState
> {
  private inputMain = React.createRef<HTMLInputElement>();

  private isPasting = false;
  constructor(props) {
    super(props);

    this.state = {
      prevValue: '',
      currentValue: '',
      isValid: true,
    };
  }

  render() {
    return (
      <input
        className={!this.state.isValid ? 'invalid' : ''}
        ref={this.inputMain}
        type="text"
        value={this.state.currentValue}
        onKeyUp={this.handleKeyup.bind(this)}
        onPaste={this.handlePaste.bind(this)}
        onChange={this.handleInputChange.bind(this)}
      />
    );
  }

  handleKeyup(event) {
    
    const key = keyboardKey.getCode(event) as number;

    // Handle backspace
    if (key == keyboardKey.Backspace) {
      this.setState({
        prevValue: this.state.currentValue,
        currentValue: '',
      });
      if (this.state.prevValue !== '') {
        return;
      }

      this.props.onInput({
        idx: this.props.idx,
        isBackwards: true,
      });

      return;
    }

    if (
      event.key != 'Meta' &&
      ((key >= 48 && key <= 57) ||
        (key >= 65 && key <= 90) ||
        (key >= 97 && key <= 122)) ||
        (key >= 186 && key <= 222)
    ) {
      this.setState({ currentValue: event.key });
      this.props.onInput({
        idx: this.props.idx,
        isBackwards: false,
      });
    } else {
      event.preventDefault();
    }
  }

  handlePaste(event) {
    console.log(event)
    this.isPasting = true;
    setTimeout(() => {
      this.props.onPaste({
        value: this.inputMain.current?.value || '',
      });
      this.isPasting = false;
    }, 5);
  }

  handleInputChange(event) {
    const val = event.target.value;
    this.setState({
      prevValue: this.state.currentValue,
      currentValue: val,
    });
  }

  blur() {
    this.inputMain.current?.blur();
  }

  focus() {
    this.inputMain.current?.focus();
  }

  setValue(value) {
    console.log({ value });
    this.setState({
      currentValue: value,
      prevValue: '',
    });
  }

  validate() {
    let isValid = true;
    if (this.state.currentValue === '' || isNaN(+this.state.currentValue)) {
      isValid = false;
    }

    this.setState({
      isValid,
    });
    return isValid;
  }

  get value() {
    return this.state.currentValue;
  }
}
