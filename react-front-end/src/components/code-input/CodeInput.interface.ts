export interface CodeInputProps {
  idx: number;
  onInput: (ev: CodeInputEvent) => void;
  onPaste: (ev: CodeInputPasteEvent) => void;
}

export interface CodeInputState {
  currentValue: string;
  prevValue: string;
  isValid: boolean;
}

export interface CodeInputEvent {
  idx: number;
  isBackwards: boolean;
}


export interface CodeInputPasteEvent {
  value: string;
}