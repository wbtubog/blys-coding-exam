import React, { RefObject } from 'react';
import './Verification.css';
import { VerificationProps } from './Verification.interface';
import CodeInput from './../code-input/CodeInput';
import {
  CodeInputEvent,
  CodeInputPasteEvent,
} from '../code-input/CodeInput.interface';

export default class Verification extends React.Component<VerificationProps> {
  private inputs: RefObject<CodeInput>[] = [];

  constructor(props) {
    super(props);
    this.inputs = Array(this.props.digits)
      .fill(0)
      .map((x, i) => React.createRef<CodeInput>());
  }

  renderCodeInputs(ref, index: number) {
    return (
      <CodeInput
        ref={ref}
        key={index}
        idx={index}
        onInput={this.handleCodeInput.bind(this)}
        onPaste={this.handlePaste.bind(this)}
      />
    );
  }

  render() {
    return <div>{this.inputs.map((v, i) => this.renderCodeInputs(v, i))}</div>;
  }

  handleCodeInput(ev: CodeInputEvent): void {
    const newIdx = ev.isBackwards ? ev.idx - 1 : ev.idx + 1;
    if (newIdx in this.inputs) {
      this.inputs[ev.idx].current?.blur();
      this.inputs[newIdx].current?.focus();
    }
  }

  handlePaste(ev: CodeInputPasteEvent) {
    const inputs = this.inputs;
    const chars = ev.value
      .split('')
      .map((v) => v.trim())
      .filter((v) => v !== '');
    setTimeout(() => {
      chars.forEach((value, index) => {
        if (index in inputs) {
          this.inputs[index].current?.setValue(value);
        }
      });

      // after paste event, focus on the last node
      this.inputs[inputs.length - 1].current?.focus();
    }, 10);
  }

  getValue() {
    const inputs = this.inputs;

    const isOneFieldInvalid = inputs
      .map((component) => {
        return component.current?.validate();
      })
      .some((status) => status == false);

    return !isOneFieldInvalid
      ? inputs.map((c) => c.current?.value).join('')
      : null;
  }
}
