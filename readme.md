# Blys Coding Exam 🎉 

I wrote the front-end for both Angular and ReactJS with the former being my main framework of choice for front-ends. I did a quick read of ReactJS' docs and decided to try and write it in React by using the basic concepts that I learned from their docs :)

Both API and Angular Front-end have unit test except for the ReactJS part since I have not explored unit testing there.

I also chose `Typescript` to be the language that I use for this stack. This'll make the code more readable because it has types and that will also serve as a documentation for developers to come 😉